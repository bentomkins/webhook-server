import { Controller, Get, Post, Body, UseGuards } from '@nestjs/common';
import { GithubGuard, GithubWebhookEvents } from '@dev-thought/nestjs-github-webhooks';
import { WebhookService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly webhookService: WebhookService) {}

  @Post('config-file-webhook')
  @GithubWebhookEvents(['pullrequest'])
  @UseGuards(GithubGuard)
  configFileWebhook(@Body() payload: any) {
    this.webhookService.getHello();
  }

  @Post('update-dependant-file-webhook')
  @GithubWebhookEvents(['pullrequest'])
  @UseGuards(GithubGuard)
  updateStaticFiles(@Body() payload: any) {
    this.webhookService.getHello();
  }
}
