import { Injectable } from '@nestjs/common';

@Injectable()
export class WebhookService {
  getHello(): void {
    console.log('WebhookService triggered!');
  }
}
