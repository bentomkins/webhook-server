import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { WebhookService } from './app.service';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { GithubWebhooksModule } from '@dev-thought/nestjs-github-webhooks';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      // returns static files from the specified root folder
      rootPath: join(__dirname, '..', 'resources'),
    }),
    GithubWebhooksModule.forRoot({
      webhookSecret: process.env.GITHUB_WEBHOOK_SECRET,
    }),
  ],
  controllers: [AppController],
  providers: [WebhookService],
})
export class AppModule {}
